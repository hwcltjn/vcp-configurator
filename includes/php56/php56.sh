#!/bin/bash
# Custom PHP Wrapper for PHP 5.6 on VestaCP - https://gitlab.com/hwcltjn/vcp-configurator
# Last Update: 07/10/2017 
# Based on Raphael Schneeberger's work - https://git.scit.ch/rs/VestaCP-MultiPHP

# Adding php wrapper
user="$1"
domain="$2"
ip="$3"
home_dir="$4"
docroot="$5"

wrapper_script='#!/usr/bin/php-cgi5.6 -c/etc/php/5.6/cgi/php.ini'
wrapper_file="/home/$user/web/$domain/cgi-bin/php"

echo "$wrapper_script" > $wrapper_file
chown $user:$user $wrapper_file
chmod -f 751 $wrapper_file

exit 0
